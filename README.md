> :heavy_exclamation_mark: 请谨慎修改 packages.yaml，一般新增包请添加至 packages_epol.yaml

# 软件包管理

- **源码包管理**：软件包按 sig 进行分类，新增仓库、修改仓库所属sig/衰退仓库、删除仓库都需要修改 rpm_info/packages.yaml 或 rpm_info/packages_epol.yaml 文件实现
    - **packages.yaml**：记录 BaseOS、Appstream 软件包信息
    - **packages_epol.yaml**：记录 EPOL 软件包信息
- **二进制包管理**：二进制包由 `baseos.list`、`appstream.list`、`deprecated.list` 四个文件管理所属源
- **仓库成员管理**：通过 `collaborators.yaml` 文件将社区开发者添加为仓库成员

## 新增软件包 
1. **自检查**：确认当前仓库及 packages.yaml 以及 packages_epol.yaml 文件中**不存在**该软件包
2. fork 仓库：https://gitee.com/OpenCloudOS/rpm_info
2. clone 仓库：`git clone git@gitee.com:<username>/rpm_info.git`
3. **源码包管理**：新增包修改 packages_epol.yaml 文件，将新增软件包名添加到合适的 sig-packages 列表尾端（若需要添加到 BaseOS/Appstream 仓库，则修改 packages.yaml 文件）
4. **二进制包管理**：若修改 packages_epol.yaml 文件，则无需手动修改；若修改 packages.yaml 文件，则请将对应二进制包添加至 `baseos.list`、`appstream.list`
4. 提交 pull request：记录**新增原因**
5. 等到流水线结果：根据修改内容返回解析结果，请检查解析结果是否符合预期
6. 人工审核
7. 自动化新增软件包，返回仓库链接


## 衰退软件包
1. **自检查**：确认当前仓库及 packages.yaml 或 packages_epol.yaml 文件中**存在**该软件包
2. fork 仓库：https://gitee.com/OpenCloudOS/rpm_info
2. clone 仓库：`git clone git@gitee.com:<username>/rpm_info.git`
3. **源码包管理**：修改 packages.yaml 或 packages_epol.yaml，将衰退的软件包移动到 deprecated sig
4. **二进制包管理**：将衰退软件包对应的所有二进制软件，从原始列表移到deprecated.list中，并确认没有其他二进制包依赖该衰退软件包。（比如，假设appstream-data这个软件需要衰退，它存在于appstream.list中，那么就需要删除appstream.list中 appstream-data 所有二进制包，添加到 deprecated.list，才算完成 appstream-data 的衰退操作。）
4. 提交 pull request：记录**衰退原因**
5. 等到流水线结果：根据修改内容返回解析结果，请检查解析结果是否符合预期
6. 人工审核
7. 自动化衰退软件包


## 删除软件包（谨慎操作！）
1. **自检查**：确认当前仓库及 packages.yaml 或 packages_epol.yaml 文件中**存在**该软件包，并且已经被**衰退**，位于 deprecated sig 中（仅允许删除 deprecated sig 中已经被衰退的软件包），并且有必要删除
2. fork 仓库：https://gitee.com/OpenCloudOS/rpm_info
2. clone 仓库：`git clone git@gitee.com:<username>/rpm_info.git`
3. **源码包管理**：修改 packages.yaml 或 packages_epol.yaml，将软件包名所在行删除
4. **二进制包管理**：将被删除软件包对应的所有二进制包，从所在列表中删除。
4. 提交 pull request：记录**删除原因**
5. 等到流水线结果：根据修改内容返回解析结果，请检查解析结果是否符合预期
6. 人工审核
7. 自动化删除软件包

## 修改软件包

### 修改软件包 sig
1. **自检查**：确认当前仓库及 packages.yaml 或 packages_epol.yaml 文件中**存在**该软件包
2. fork 仓库：https://gitee.com/OpenCloudOS/rpm_info
2. clone 仓库：`git clone git@gitee.com:<username>/rpm_info.git`
3. **源码包管理**：修改 packages.yaml 或 packages_epol.yaml，将软件包从原始sig中删除，并添加到新sig中
4. **二进制包管理**：仅修改软件包 sig，无需修改二进制包
4. 提交 pull request：记录**修改原因**
5. 等到流水线结果：根据修改内容返回解析结果，请检查解析结果是否符合预期
6. 人工审核
7. 自动化修改软件包 sig


### 移动软件包所在源

> :heavy_exclamation_mark: 默认不允许将软件包从 EPOL 移入 BaseOS/Appstream，以下只考虑将软件包从 BaseOS/Appstream 移入 EPOL 的情况

> 参考pr：https://gitee.com/OpenCloudOS/rpm_info/pulls/250

1. **自检查**：确认当前仓库及 packages.yaml 或 packages_epol.yaml 文件中**存在**该软件包
2. fork 仓库：https://gitee.com/OpenCloudOS/rpm_info
2. clone 仓库：`git clone git@gitee.com:<username>/rpm_info.git`
3. **源码包管理**：将对应软件包从 packages.yaml 中删除，添加到 packages_epol.yaml；反之依然。 
4. **二进制包管理**：将对应二进制包列表从 `baseos.list`、`appstream.list` 删除，添加到 deprecated.list
4. 提交 pull request：记录**修改原因**
5. 等到流水线结果：根据修改内容返回解析结果，请检查解析结果是否符合预期
6. 人工审核
7. 自动化新增软件包，返回仓库链接

### 修改二进制包所在源

> :heavy_exclamation_mark: 默认不允许将软件包从 EPOL 移入 BaseOS/Appstream

1. **自检查**：确认当前仓库及 packages.yaml 或 packages_epol.yaml 文件中**存在**该软件包
2. fork 仓库：https://gitee.com/OpenCloudOS/rpm_info
2. clone 仓库：`git clone git@gitee.com:<username>/rpm_info.git`
3. **源码包管理**：无需修改 packages.yaml 或 packages_epol.yaml
4. **二进制包管理**：确认移动后不影响其他软件包的安装，从原始列表移到新列表中。（例如，BaseOS 中的软件包不能依赖其他源中的软件包，AppStream 中的软件包仅依赖 BaseOS + AppStream 源）
4. 提交 pull request：记录**修改原因**
5. 等到流水线结果：根据修改内容返回解析结果，请检查解析结果是否符合预期
6. 人工审核
7. 自动化移动软件包所在源


## 加入仓库

我们欢迎并鼓励开发者参与我们的仓库开发，若您想参与社区仓库的维护工作，或体验自动升级流程，您需先申请成为仓库成员。为此，我们通过名为`collaborators.yaml`的文件将社区开发者添加为仓库成员。

要申请成为仓库开发者，请按照以下步骤操作：

1. fork 仓库：`https://gitee.com/OpenCloudOS/rpm_info`
2. clone 仓库：`git clone git@gitee.com:<username>/rpm_info.git`
3. 编辑 `collaborators.yaml` 文件，添加您的信息。请确保正确填写您的 gitee 账号ID、邮箱和希望加入的仓库名称。
4. 提交 pull request 并**提供修改原因**


第一次申请加入仓库时，需要填写 Gitee 账户名称，请以该模板为参考，在 `collaborators.yaml` 文件末尾追加以下内容：
```yaml
- 账号ID: xxx
  仓库:
    - xxx
    - xxx
```

示例：
```yaml
- 账号ID: gitee-bot
  仓库:
    - setup
```

字段说明：

- `账号ID`：Gitee 账户名称，**请务必保证与 gitee 账号ID一致**，否则无法正确为您修改权限。以 [gitee-bot](https://gitee.com/gitee-bot) 用户为例，个人主页中，左上角个人介绍中 `@gitee-bot` 表示该账号ID为 `gitee-bot`，与个人空间地址一致，而 `Gitee GPG Bot` 则是账户昵称，非唯一标识。
- `仓库`：申请加入的仓库列表。


注意事项：

- 在修改`collaborators.yaml`文件时，**请不要修改其他成员的信息。**；若团队参与贡献，也可直接联系我们申请仓库权限
- 如有任何疑问或需要帮助，请随时与我们联系。

## 常见问题
- 某些软件包无法解析
    - 如果无法解析软件包，请检查 **自检查** 条件是否满足，以及修改内容是否包含空格等无效字符，若仍然有问题请提issue方便后续跟踪处理
- 能否同时增加、删除、修改软件包
    - 可以。请确认流水线返回解析结果符合预期即可。此外建议一个 PR 只做一件事情